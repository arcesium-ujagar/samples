# This file is a template, and might need editing before it works on your project.
FROM 674283286888.dkr.ecr.us-east-1.amazonaws.com/core-infra/openjdk:11

WORKDIR /app
COPY code-0.0.1-SNAPSHOT.jar /app/app.jar

EXPOSE 8081

# Define environment variable
ENV NAME testapp-ujagar
ENTRYPOINT ["java","-jar","/app.jar"]

