package com.arcesium.java.sample.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HomeController {

    @GetMapping("/home")
    public String homePage() {
        return "Welcome to sample spring boot app";
    }


    @GetMapping("/isHealthy")
    public boolean isHealthy() {
        return true;
    }

}
